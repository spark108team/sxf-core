<?php

namespace SXFCore\Db\Schema;

use XF\Db\SchemaManager;

class Alter extends XFCP_Alter
{
	protected $triggerDefinitions;
	
	public function __construct(\XF\Db\AbstractAdapter $db, SchemaManager $sm, $tableName)
	{
		parent::__construct($db, $sm, $tableName);

		$this->triggerDefinitions = $sm->getTableTriggerDefinitions($tableName);
	}
	
	public function dropTrigger($triggerNames)
	{
		foreach ((array)$triggerNames AS $triggerName)
		{
			$triggerDef = $this->getTriggerDefinition($triggerName);
			if (!$triggerDef)
			{
				// trigger already dropped/doesn't exist so skip it
				continue;
			}

			//$trigger = $this->changeTrigger($triggerName);
			//$column = $this->changeColumn($columnName);
			//$column->drop();
		}

		return $this;
	}
	
	public function getTriggerDefinition($triggerName)
	{
		$definitions = $this->triggerDefinitions;
		return isset($definitions[$triggerName]) ? $definitions[$triggerName] : null;
	}

	public function getTriggerDefinitions()
	{
		return $this->triggerDefinitions;
	}
}
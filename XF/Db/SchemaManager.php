<?php

namespace SXFCore\XF\Db;

class SchemaManager extends XFCP_SchemaManager
{
	public function getTableTriggerDefinitions($tableName)
	{
		$config = \XF::config();
		$dbName = $config['db']['dbname'];

		return $this->db->fetchAllKeyed('
			SHOW TRIGGERS FROM `' . $dbName . '` WHERE `Table`=\'' . $tableName . '\'
		', 'Field');
	}
}
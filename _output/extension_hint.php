<?php

// ################## THIS IS A GENERATED FILE ##################
// DO NOT EDIT DIRECTLY. EDIT THE CLASS EXTENSIONS IN THE CONTROL PANEL.

namespace SXFCore\XF\Db
{
	class XFCP_SchemaManager extends \XF\Db\SchemaManager {}
}

namespace SXFCore\XF\Db\Schema
{
	class XFCP_Alter extends \XF\Db\Schema\Alter {}
}

namespace SXFCore\XF\Entity
{
	class XFCP_User extends \XF\Entity\User {}
}

namespace SXFCore\XF\Pub\Controller
{
	class XFCP_Account extends \XF\Pub\Controller\Account {}
}

namespace SXFCore\XF\Template
{
	class XFCP_Templater extends \XF\Template\Templater {}
}